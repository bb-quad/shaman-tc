function temp(input)
  cmds = {}
  for s in input:gmatch("%S+") do
    table.insert(cmds, s)
  end

  cmd = cmds[1]
  if (cmd == "l" or cmd == "list") then
    list()
  elseif (cmd == "a" or cmd == "announce") then
    buffs = cmds[2]
    chat = cmds[3] or "ra" -- defaults to /ra
    announce(buffs, chat)
  if (cmd == "r" or cmd == "remove") then
    remove()
  if (cmd == "u" or cmd == "update") then
    update()
  elseif (cmd == "h" or cmd == "help") then
    help()
  else
    print(string.format("%s is not a recognised command. Use 'help' for more info.", cmd))
  end
end

function help()
  print("Arguments:")
  print("  list | l")
  print("    List Greater Protection Potions for each raid member.")
  print("  annouce <list> [chat] | a <list> [chat]")
  print("    Annouce missing buffs from buff list 'b' to 'chat'.")
  print("    <list> is a comma separated list of buffs. E.g. gnpp,gspp for greater shadow and nature protection")
  print("    Supported options: gnpp|n, gspp|s, gapp|a")
  print("    'chat' defaults to 'ra'. Supported options: 'ra', 'rw', 'self'")
  print("  help | h")
  print("    This message.")
end

function list()
  print("'list' not implemented yet.")
  -- create list of raid members and their gxpp buffs
  -- print list (later create GUI frame)
end

-- inverse of list()
function remove()
  print("'remove' not implemented yet.")
end

-- manually force update list. For GUI purpose only, until proper event hooks have been implemented
function update()
  print("'update' not implemented yet.")
end

buffIdMap = { -- TODO: use correct spell ids!
  "n" = 12345,
  "gnpp" = 12345,
  "m" = 1234 -- motw for debuging
}

function announce(buffs, chat)
  -- pass buffs string
  possibleBuffs = {}
  for b in buffs:gmatch("%a") do
    table.insert(possibleBuffs, b)
  end

  -- validate input
  for p in possibleBuffs do
    if buffIdMap[p] == nil then
      print(string.format("Buff string '%s' not supported", p))
      return
    end
  end

  -- map buff string to spellIds
  buffIds = {}
  for p in possibleBuffs do
    buffIds[buffIdMap[p]] = true
  end

  if IsInRaid("player") == false then -- TODO: use correct method, see API docs
    print("Not in raid.")
    return
  end

  -- scan raid1 .. raid40 for spellIds
  -- and create output
  missingBuffs = {}
  for buffId in buffIds do
    name = GetSpellInfo(buffId) -- TODO: use correct method, see API docs
    missingBuffs[name] = {}
    for i in 1,40 do
      if (IsRaidMmeberMissingBuff(i, buffId) == false) then
        raidMember = GetRaidMemberInfo(i) -- TODO: use correct method, see API docs
        table.insert(missingBuffs[name], raidMember)
      end
    end
  end

  -- print to chat
end

-- raidId not present in raid => false, since non-existing members cannot be missing buffs
function IsRaidMmeberMissingBuff(raidId, spellId)
end
