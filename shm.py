"""
" Requirements:
" * Python 3.7 or newer
"
"""
from dataclasses import dataclass
import matplotlib.pyplot as plt

# types
@dataclass
class Spell:
    name: str
    heal: float
    cost: float
    cast_time: float
    details: str = None

    def __str__(self):
        return columns()

@dataclass
class Stats:
    healing: int
    crit: float

# spell transformations
def healing(spell, stats):
    return spell.heal * 1.1 + spell.cast_time / 3.5 * stats.healing

def healing_wave(spell, stats, t1_5set = False, targets = 1):
    heal = healing(spell, stats) * (1.18 if targets == 1 else 1)
    heals = [heal] + [heal * (0.2 ** i) for i in range(1, targets)]
    total_heal = sum(heals) * (1 + 0.5 * stats.crit)

    mana = spell.cost * 0.95 - (spell.cost * 0.25 * 0.35 if t1_5set else 0)
    details = f'Targets={targets} (T1 8set)' if targets > 1 else ''
    return Spell(spell.name, total_heal, mana, spell.cast_time - 0.5, details)

def chain_heal(spell, stats, t2_bonus = False, t25_bonus = False, targets=3):
    coef = 0.5 if t2_bonus is False else 0.5 * 1.3

    heal = healing(spell, stats)
    heals = [heal] + [heal * (coef ** i) for i in range (1, targets)]
    total_heal = sum(heals) * (1 + 0.5 * stats.crit)

    ct = spell.cast_time - (0.4 if t25_bonus else 0)

    details = (f'Targets={targets} ' if targets > 1 else '' +
        '(T2 3set) ' if t2_bonus else '' +
        '(T2.5 5set)' if t25_bonus else '')
    return Spell(spell.name, total_heal, spell.cost * 0.95, ct, details)

# util functions

def columns(*cols, width=10):
    format_str = f'{{:{width}}}'*len(cols)
    return format_str.format(*cols)

hw5 = Spell('HW5', (389+454)/2, 200, 3)
hw7 = Spell('HW7', (759+874)/2, 340, 3)
hw9 = Spell('HW9', (1389+1583)/2, 560, 3)
ch1 = Spell('CH1', (332+381)/2, 260, 2.5)
ch3 = Spell('CH3', (567+646)/2, 405, 2.5)

def plot_healing(targets):
    stats = [Stats(n, 0.13) for n in range(500, 1200, 25)]


    # plot results
    fig, ax = plt.subplots(1, 2, figsize=(12,7))
    ax[0].set_ylabel('hps')
    ax[0].set_xlabel('+healing')
    ax[1].set_ylabel('hpm')
    ax[1].set_xlabel('+healing')

    x = [s.healing for s in stats]

    # CH1
    tmp = [chain_heal(ch1, s, t2_bonus = True, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='CH1')
    ax[1].plot(x, y_hpm, label='CH1')

    tmp = [chain_heal(ch1, s, t2_bonus = True, t25_bonus = True, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='CH1, T2.5')

    # CH3
    tmp = [chain_heal(ch3, s, t2_bonus = True, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='CH3')
    ax[1].plot(x, y_hpm, label='CH3')

    tmp = [chain_heal(ch3, s, t2_bonus = True, t25_bonus = True, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='CH3. T2.5')

    # HW5
    tmp = [healing_wave(hw5, s, t1_5set = False, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='HW5')
    ax[1].plot(x, y_hpm, label='HW5')

    tmp = [healing_wave(hw5, s, t1_5set = True, targets = targets) for s in stats]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[1].plot(x, y_hpm, label='HW5, 5set T1')

    # HW7
    tmp = [healing_wave(hw7, s, t1_5set = False, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='HW7')
    ax[1].plot(x, y_hpm, label='HW7')

    tmp = [healing_wave(hw7, s, t1_5set = True, targets = targets) for s in stats]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[1].plot(x, y_hpm, label='HW7, 5set T1')

    # HW9
    tmp = [healing_wave(hw9, s, t1_5set = False, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='HW9')
    ax[1].plot(x, y_hpm, label='HW9')

    tmp = [healing_wave(hw9, s, t1_5set = True, targets = targets) for s in stats]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[1].plot(x, y_hpm, label='HW9, 5set T1')

    ax[0].legend()
    ax[1].legend()
    fig.suptitle(f'Targets = {targets}')
    plt.savefig(f'healing_{targets}targets.png', bbox_inches='tight')

def multi_targets():
    targets = 3
    # t2.5 + t2
    t25_healing = 192
    offset_healing = 477
    t1_healing = 125

    crit = 0.13
    stats = [Stats(n, crit) for n in range(300, 600, 25)]
    x = [s.healing for s in stats]


    # plot results
    fig, ax = plt.subplots(1, 2, figsize=(12,7))
    ax[0].set_ylabel('hps')
    ax[0].set_xlabel('+healing*')
    ax[1].set_ylabel('hpm')
    ax[1].set_xlabel('+healing*')


    # CH1
    stats = [Stats(n + t25_healing, crit) for n in range(300, 600, 25)]
    tmp = [chain_heal(ch1, s, t2_bonus = True, t25_bonus = True, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='CH1')
    ax[1].plot(x, y_hpm, label='CH1')

    # CH3
    stats = [Stats(n + t25_healing, crit) for n in range(300, 600, 25)]
    tmp = [chain_heal(ch3, s, t2_bonus = True, t25_bonus = True, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='CH3')
    ax[1].plot(x, y_hpm, label='CH3')

    # HW5
    stats = [Stats(n + t1_healing, crit) for n in range(300, 600, 25)]
    tmp = [healing_wave(hw5, s, t1_5set = True, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='HW5')
    ax[1].plot(x, y_hpm, label='HW5')

    # HW7
    stats = [Stats(n + t1_healing, crit) for n in range(300, 600, 25)]
    tmp = [healing_wave(hw7, s, t1_5set = True, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='HW7')
    ax[1].plot(x, y_hpm, label='HW7')

    # HW9
    stats = [Stats(n + t1_healing, crit) for n in range(300, 600, 25)]
    tmp = [healing_wave(hw9, s, t1_5set = True, targets = targets) for s in stats]
    y_hps = [s.heal / s.cast_time for s in tmp]
    y_hpm = [s.heal / s.cost for s in tmp]
    ax[0].plot(x, y_hps, label='HW9')
    ax[1].plot(x, y_hpm, label='HW9')


    ax[0].legend()
    ax[1].legend()
    #fig.suptitle(f'Targets = {targets}')
    plt.savefig(f'multiple_targets.png', bbox_inches='tight')

# main
if (__name__ == '__main__'):
    for i in range(1,4):
        plot_healing(i)

    multi_targets()








