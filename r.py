"""
" Requirements:
" - Python 3.7 or newer
" - Matplotlib
"
"""
from dataclasses import dataclass
import matplotlib.pyplot as plt

# types
@dataclass
class Spell:
    name: str
    level: int
    cost: int
    cast_time: float
    heal: float = 0
    hot: int = 0

    def __str__(self):
        return columns()

@dataclass
class Stats:
    healing: int
    crit: float
    t2_5set: bool = False
    t2_8set: bool = False
    relic_ht: bool = False
    relic_rejuv: bool = False

@dataclass
class Talents:
    tranquil_spirit: int = 0
    gift_of_nature: int = 0
    imp_rejuv: int = 0
    imp_regrowth: int = 0
    moonglow: int = 0
    

@dataclass
class Rejuv_data:
    name: str
    base_heal: int = 0
    bonus_heal: int = 0
    talent_heal: int = 0

    def healing(self):
        return [int(self.base_heal), 
                int(self.talent_heal), 
                int(self.bonus_heal)]

    def healing_headers(self):
        return ['base', 
                'talent', 
                'bonus']

@dataclass
class Touch_data:
    name: str
    base_heal: int = 0
    bonus_heal: int = 0
    talent_heal: int = 0
    crit_heal: int = 0

    def healing(self):
        return [int(self.base_heal), 
                int(self.talent_heal), 
                int(self.bonus_heal),
                int(self.crit_heal)]

    def healing_headers(self):
        return ['base', 
                'talent', 
                'bonus',
                'crit']


@dataclass
class Regrowth_data:
    name: str
    direct_base_heal: int = 0
    direct_talent_heal: int = 0
    direct_bonus_heal: int = 0
    hot_base_heal: int = 0
    hot_bonus_heal: int = 0
    hot_talent_heal: int = 0
    crit_heal: int = 0

    def healing(self):
        return [int(self.direct_base_heal), 
                int(self.hot_base_heal), 
                int(self.direct_talent_heal), 
                int(self.hot_talent_heal),
                int(self.direct_bonus_heal),
                int(self.hot_bonus_heal),
                int(self.crit_heal)]

    def healing_headers(self):
        return ['direct base', 
                'hot base', 
                'direct talent', 
                'hot talent',
                'direct bonus',
                'hot bonus',
                'crit']


# spell data

rg = [  Spell(name='RG1', level=12, heal=(93+107)/2, hot=98, cost=120, cast_time=2),
        Spell(name='RG2', level=18, heal=(176+201)/2, hot=175, cost=205, cast_time=2),
        Spell(name='RG3', level=24, heal=(255+290)/2, hot=259, cost=280, cast_time=2),
        Spell(name='RG4', level=30, heal=(336+378)/2, hot=343, cost=350, cast_time=2),
        Spell(name='RG5', level=36, heal=(425+478)/2, hot=427, cost=420, cast_time=2),
        Spell(name='RG6', level=42, heal=(534+599)/2, hot=546, cost=510, cast_time=2),
        Spell(name='RG7', level=48, heal=(672+751)/2, hot=686, cost=615, cast_time=2),
        Spell(name='RG8', level=54, heal=(839+935)/2, hot=861, cost=740, cast_time=2),
        Spell(name='RG9', level=60, heal=(1003+1119)/2, hot=1064, cost=880, cast_time=2)]

rj = [  Spell(name='RJ1', level=4, hot=32, cost=25, cast_time=1.5),
        Spell(name='RJ2', level=10, hot=56, cost=40, cast_time=1.5),
        Spell(name='RJ3', level=16, hot=116, cost=75, cast_time=1.5),
        Spell(name='RJ4', level=22, hot=180, cost=105, cast_time=1.5),
        Spell(name='RJ5', level=28, hot=244, cost=135, cast_time=1.5),
        Spell(name='RJ6', level=34, hot=304, cost=160, cast_time=1.5),
        Spell(name='RJ7', level=40, hot=388, cost=195, cast_time=1.5),
        Spell(name='RJ8', level=46, hot=488, cost=235, cast_time=1.5),
        Spell(name='RJ9', level=52, hot=608, cost=280, cast_time=1.5),
        Spell(name='RJ10', level=58, hot=756, cost=335, cast_time=1.5),
        Spell(name='RJ11', level=60, hot=888, cost=360, cast_time=1.5)]

ht = [  Spell(name='HT1', level=1, heal=(40+55)/2, cost=25, cast_time=1.5),
        Spell(name='HT2', level=8, heal=(94+119)/2, cost=55, cast_time=2),
        Spell(name='HT3', level=14, heal=(204+253)/2, cost=110, cast_time=2.5),
        Spell(name='HT4', level=20, heal=(376+459)/2, cost=185, cast_time=3),
        Spell(name='HT5', level=26, heal=(589+712)/2, cost=270, cast_time=3.5),
        Spell(name='HT6', level=32, heal=(762+914)/2, cost=335, cast_time=3.5),
        Spell(name='HT7', level=38, heal=(958+1143)/2, cost=405, cast_time=3.5),
        Spell(name='HT8', level=44, heal=(1125+1453)/2, cost=495, cast_time=3.5),
        Spell(name='HT9', level=50, heal=(1545+1826)/2, cost=600, cast_time=3.5),
        Spell(name='HT10', level=56, heal=(1916+2257)/2, cost=720, cast_time=3.5),
        Spell(name='HT11', level=60, heal=(2267+2677)/2, cost=800, cast_time=3.5)]

# util functions

def columns(*cols, width=10):
    formatStr = f'{{:{width}}}'*len(cols)
    return formatStr.format(*cols)

def levelPenalty(level):
    if level < 20:
        return 1 - ((20 - level) * .0375)
    else:
        return 1

# map functions

def rejuv(spell, stats, talents):
    coef = 0.8 * levelPenalty(spell.level)
    res = Rejuv_data(spell.name)

    res.base_heal = spell.hot
    res.talent_heal = spell.hot * (1 + 0.05 * talents.imp_rejuv) * (1 + 0.02 * talents.gift_of_nature) - spell.hot
    res.bonus_heal = coef * stats.healing

    return res

def ht(spell, stats, talents):
    coef = spell.cast_time/3.5 * levelPenalty(spell.level)
    res = Touch_data(spell.name)

    res.base_heal = spell.heal
    res.talent_heal = spell.heal * (0.02 * talents.gift_of_nature)
    res.bonus_heal = coef * stats.healing
    total = res.base_heal + res.talent_heal + res.bonus_heal
    res.crit = 0.5 * total * stats.crit

    return res

def regrowth(spell, stats, talents):
    direct_coef = 0.325 * levelPenalty(spell.level)
    hot_coef = 0.513 * levelPenalty(spell.level)
    res = Regrowth_data(spell.name)

    res.direct_base_heal = spell.heal
    res.direct_talent_heal = spell.heal * (0.02 * talents.gift_of_nature)
    res.direct_bonus_heal = stats.healing * direct_coef
    res.hot_base_heal = spell.hot
    res.hot_talent_heal = spell.hot * (0.02 * talents.gift_of_nature)
    res.hot_bonus_heal = spell.hot * hot_coef
    direct_total = res.direct_base_heal + res.direct_talent_heal + res.direct_bonus_heal
    crit_chance = stats.crit + 0.1 * talents.imp_regrowth
    res.crit_heal = 0.5 * direct_total * crit_chance

    return res

def regen(time):
    """
    input: time in seconds
    return: mana regened over <time> via different mechanisms
    """
    intel = 15
    mp5 = 1/5*time
    inner = 4/4.5*15
    spirit = 1/4.5 * 0.15 * time
    spirit_inner = 1/4.5 * 0.15 * (time - 15) + inner
    spirit_3t2 = 1/4.5 * 0.3 * time
    spirit_3t2_inner = 1/4.5 * 0.3 * (time - 15) + inner

    return (intel, mp5, spirit, spirit_inner, spirit_3t2, spirit_3t2_inner)

def plot_data_stacked_bar(spell_data, title, ax):
    names = [s.name for s in spell_data]
    headers = spell_data[0].healing_headers()
    elems = [s.healing() for s in spell_data]
    data = [list(x) for x in zip(*elems)]

    offset = [0]*len(names)
    for i, d in enumerate(data):
        ax.barh(names, d, left=offset, label=headers[i])
        offset = [sum(x) for x in zip(offset, d)]

    ax.set_yticks(names)
    ax.set_xlabel('Healing')
    ax.set_title(title)
    ax.legend()

#
if (__name__ == '__main__'):

    mend = Talents(tranquil_spirit=5, gift_of_nature=5, imp_rejuv=3, imp_regrowth=5, moonglow=0)
    current_gear = Stats(800, 0.07)

    rg_data = [regrowth(s, current_gear, mend) for s in rg]

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    plot_data_stacked_bar(rg_data, 'Regrowth healing', ax)
    plt.tight_layout()
    plt.savefig('regrowth_healing.png')

    for i in range(60, 300, 60):
        print(regen(i))

